import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入vuex
import store from '@/store/index.js'
// 引入支持ES6的包
import 'babel-polyfill'
// 引入css，重定义样式 解决移动端1px像素问题 iconfont
import '@/assets/css/reset.css'
import '@/assets/css/border.css'
import '@/assets/css/iconfont.css'

// 引入第三方包
  // 解决移动端300ms点击延迟问题
import fastClick from 'fastclick'
fastClick.attach(document.body)
// 引入vue-awesome-swiper@2.6.7
import VueAwesomeSwiper from 'vue-awesome-swiper'
require('swiper/dist/css/swiper.css')
Vue.use(VueAwesomeSwiper)

import axios from 'axios'
Vue.prototype.$http=axios
axios.defaults.baseURL = '/api/'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
