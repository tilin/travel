import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {path: '/',name: 'Home',component: () => import('@/views/Home.vue')},
  {path: '/city',name: 'City',component: () => import('@/views/City.vue')},
  {path: '/detail/:id',name: 'Detail',component: () => import('@/views/Detail.vue')},
]

const router = new VueRouter({
  routes,
  // 解决当页面滑动到下方时点击进入详情页不在顶部问题
  scrollBehavior: function (to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})

export default router
