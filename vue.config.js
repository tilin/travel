// 配置完成后需重启项目
module.exports={
    devServer:{
        proxy:{
            '/api':{
                target:'http://localhost:8080/', //指向本地地址
                changOrigin:true,
                pathRewrite:{
                    '^/api':'/mock' //路径转发代理
                }
            }
        }
    }
}